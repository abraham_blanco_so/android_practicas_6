package com.sitiouno.memoria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void guardarPreferencia(View v){

        SharedPreferences miPreferenciaCompartida = getSharedPreferences("MisDatosQueFrao",Context.MODE_PRIVATE);

        SharedPreferences.Editor Editor = miPreferenciaCompartida.edit();
        EditText edtNombre = (EditText) findViewById(R.id.edtNombre);
        EditText edtCorreo = (EditText) findViewById(R.id.edtCorreo);

        String nombre = edtNombre.getText().toString();
        String correo = edtCorreo.getText().toString();

        Editor.putString("nombre",nombre);
        Editor.putString("correo",correo);

        Editor.commit();

        Toast.makeText(getApplicationContext(),"El chivo de preferencia esta creado",Toast.LENGTH_LONG).show();
        edtNombre.setText("");
        edtCorreo.setText("");
    }

    public void mostrarPreferencia(View v){

        SharedPreferences miPreferenciaCompartida = getSharedPreferences("MisDatosQueFrao",Context.MODE_PRIVATE);

        String nombre = miPreferenciaCompartida.getString("nombre","no existe nombre");
        String correo = miPreferenciaCompartida.getString("correo","no existe correo");

        TextView tvPreferenciaCompartida = (TextView)findViewById(R.id.tvPreferenciaCompartida);
        String preferencia = "\nNombre: " + nombre + "\nCorreo: " + correo;

        tvPreferenciaCompartida.setText(preferencia);

    }

    public void generarArchivo(View v){

        try {
            //Instancia el Editext de la vista en donde lo capturo y obtengo el string
            EditText edtNombre = (EditText)findViewById(R.id.edtNombre);
            String nombre      = edtNombre.getText().toString() + " ";

            //Declaro un archivo, digo que lo voy a escribir y le grabo el valor que capture en el objeto Editext de la vista
            FileOutputStream outputStream = null;
            outputStream = openFileOutput("Miarchivito.txt", Context.MODE_APPEND);
            outputStream.write(nombre.getBytes());
            outputStream.close();

            Toast.makeText(getApplicationContext(),"El chivo esta creado",Toast.LENGTH_LONG).show();
            edtNombre.setText("");

        }catch (Exception e){

          e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Cotufa escribiendo el chivo",Toast.LENGTH_LONG).show();
        }

    }
}
